﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_1_RP
{
    class NoteTime:Note{

        private DateTime CreatedAtTime;

        public NoteTime() : base() {

            CreatedAtTime = DateTime.Now;

        }


        public NoteTime(string Author, string Text, int Priority) : base( Author, Text, Priority) {
            CreatedAtTime = DateTime.Now;


        }


        public override string ToString()
        {
            return (base.ToString() + "    Created at: " + CreatedAtTime.ToString());
        }













    }
}
