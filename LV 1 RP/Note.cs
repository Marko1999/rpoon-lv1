﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_1_RP
{
    class Note{

        private string mAuthor;
        private string mText;
        private int mPriority=1;   //Priority range 1(max priority)-5(min priority)

        public string Text {
            get { return this.mText; }
            set { this.mText = Text; }

        }

        public int Priority
        {
            get { return this.mPriority; }
            set { this.mPriority = Priority; }

        }


        public Note() {
            this.mAuthor = "Unknown";
            this.mText = "Empty note";
            this.mPriority = 5;



        }

        public Note(string Author,string Text,int Priority){
            this.mAuthor = Author;
            this.mText = Text;
            this.mPriority = Priority;

        }


        public Note(string Author){
            this.mAuthor = Author;
            this.mText = "Empty Note";
            this.mPriority =5;

        }


        public override string ToString()
        {
            return ("Author name: "+this.mAuthor + "        " + "Note text: "+this.mText +"        "+ "Priority: "+this.mPriority.ToString());
        }




    }
}
